<?php
function load_page($loc,$params=array(),$addTemplates=TRUE,$checkLogin=TRUE){
	$CI=&get_instance();
	if($checkLogin and !loggedIn()){redirect("login");}
	if(!isset($params['res'])){$params['res']=$CI;}
	if($addTemplates){$CI->load->view("template/header",$params);}
	$CI->load->view($loc,$params);
	if($addTemplates){$CI->load->view("template/footer",$params);}
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}