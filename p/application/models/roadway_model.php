<?php
class Roadway_model extends CI_Model{
	function getAllRoadways($mode="object"){
		if ($mode=="object")
			return $this->db->get("roadway")->result();
		else
			return $this->db->get("roadway")->result_array();
	}
	
	function deleteRoadway($id){
		return $this->db->delete("roadway",array("ID"=>$id));
	}
	function existsRoadwayBetween($a,$b){
		
		$SQL="SELECT * from roadway where (point1=$a and point2=$b) or (point2=$a and point1=$b) LIMIT 0,1";
		$res= $this->db->query($SQL);
		return ($res->num_rows()>0);
	}
	function existsRoadway($a){
		$this->db->where("ID",$a);
		$count=$this->db->get("roadway")->result();
		return sizeof($count)>0;
	}
	function split_roadway($roadwayID,$breaker){
		//get point2 of the roadway
		//update point2=$breaker
		//add new roadway $breaker->$point2
		
		$points=$this->getLocationsOfRoadway($roadwayID);
		$SQL="insert into roadway (point1,point2) values ($breaker,$points[1])";
		$this->db->query($SQL);
		$SQL="UPDATE roadway set point2=$breaker where ID=$roadwayID";
		$this->db->query($SQL);
	}
	
	function getAllRoadwaysInProperForm(){
		$SQL="SELECT * FROM roadway";
	}
	function getRoadwayIdContaining($a,$b){
		//returns roadway ID containing $a and $b in order. if not found, return 0
		$this->db->where("point1",$a);
		$this->db->where("point2",$b);
		$this->db->select("ID");
		
		$res=$this->db->get("roadway");
		if($res->num_rows()>0){
			$result=$res->result();
			
			return $result[0]->ID;
		}
		return 0;
	}
	
	function add($data){
		return $this->db->insert("roadway",$data);
	}
	function getLocationsOfRoadway($roadwayID){
		$ret=$this->db->get_where("roadway",array("ID"=>$roadwayID))->result();
		if(!$ret)return NULL;
		return array($ret[0]->point1,$ret[0]->point2);
	}
	
	function getAllRoadwayIdForLocation($locationID){
		//returns all roadway IDs for roadways for which either of the point is $locationID
		$this->db->or_where("point1",$locationID);
		$this->db->or_where("point2",$locationID);
		$this->db->select("ID");
		$res=$this->db->get("roadway");
		$ret=array();
		foreach ($res->result() as $item)array_push($ret,$item->ID);
		return $ret;
	}
	function getAllPlacesConnectedTo($place){
		$ret=array();
		
		$this->db->where("point1",$place);
		$this->db->select("point2 as point");
		$result1=$this->db->get("roadway");
		
		$this->db->select("point1 as point");
		$this->db->where("point2",$place);
		$result2=$this->db->get("roadway");
		
		foreach($result1->result() as $row){
			if(!isset($ret[$row->point]))$ret[$row->point]=true;
		}
		foreach($result2->result() as $row){
			if(!isset($ret[$row->point]))$ret[$row->point]=true;
		}
		
		$myReturn=array();
		
		
		foreach($ret as $item=>$abc){
			$location=get_instance()->locationHandler->getLocationByID($item);
			//print_r($location);
			$curArray=array();
			$curArray['ID']=$location->ID;
			$curArray['name']=$location->name;
			$curArray['latitude']=$location->latitude;
			$curArray['longitude']=$location->longitude;
			$curArray['district']=$location->district;
			
			
			array_push($myReturn,$curArray);
		}
		
		
		//print $str;
		return $myReturn;
	}
	
	
}
?>