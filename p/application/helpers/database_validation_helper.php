<?php

/*used in setting rules during form validation */
function exists_route($id){
	$CI=&get_instance();
	return $CI->routeHandler->existsRoute($id);
}

function exists_roadway($id){
	$CI=&get_instance();
	return $CI->roadwayHandler->existsRoadway($id);
}

function exists_cost($id){
	$CI=&get_instance();
	return $CI->costHandler->exists($id);
}

function exists_vehicleType($id){
	$CI=&get_instance();
	return $CI->vehicleTypeHandler->existsType($id);
}
function exists_vehicle($id){
	return get_instance()->vehicleHandler->existsVehicle($id);
}

function exists_location($id){
	return get_instance()->locationHandler->existsLocation($id);
}

function exists_popular_place($id){
	return get_instance()->popularPlaceHandler->exists($id);
}
?>