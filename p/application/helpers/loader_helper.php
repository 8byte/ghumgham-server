<?php

$CI=&get_instance();
//load common data for all controllers
$CI->load->library('session');

$CI->load->database();
$CI->load->helper("html");
$CI->load->helper("url");
$CI->load->helper("page");
$CI->load->helper("login");
$CI->load->library("form_validation");
$CI->load->helper("database_validation");
$CI->load->helper("text_format");
$CI->load->helper("html_format");
$CI->load->model(array(
	"location_model",
	"route_model",
	"routeLocation_model",
	"vehicle_model",
	"vehicletype_model",
	"popular_place_model",
    "popular_place_image_model",
	"cost_model",
    "user_model",
	"roadway_model"));

$CI->load->helper("map");

$CI->locationHandler=new Location_model();
$CI->routeHandler=new Route_model();
$CI->vehicleTypeHandler=new VehicleType_model();
$CI->vehicleHandler=new Vehicle_model();
$CI->roadwayHandler=new Roadway_model();
$CI->routeLocationHandler=new RouteLocation_model();
$CI->popularPlaceHandler=new Popular_Place_model();
$CI->popularPlaceImageHandler=new Popular_Place_Image_model();
$CI->costHandler=new Cost_model();
$CI->userHandler=new User_model();

$CI->popularCategories=array(
	"Temple"=>"Temple",
	"Hospital"=>"Hospital",
	"Hotels/Resturant"=>"Hotels and Resturants",
	"Airlines"=>"Airlines",
	"College"=>"College",
	"Parks and related visiting places"=>"Parks and related visiting places",
	"Monasteries"=>"Monasteries"
	);


?>