<?php
class Popular_Place_model extends CI_Model{
	function __construct(){
		parent::__construct();
		
	}
	function add($data){
		$this->db->insert("popular_place",$data);
        return $this->db->insert_id();
	}
	function delete($id){
		$this->db->where("ID",$id);
		return $this->db->delete("popular_place");
	}
	function getAll(){
		return $this->db->get("popular_place")->result();
	}
	
	function exists($id){
		$this->db->where("ID",$id);
		return ($this->db->get("popular_place")->num_rows()>0);
	}
	function update($id,$data){
		$this->db->where("ID",$id);
		return $this->db->update("popular_place",$data);
	}
    function ofCat($cat){
        $this->db->where("category",$cat);
        return $this->db->get("popular_place")->result();
    }
}

?>