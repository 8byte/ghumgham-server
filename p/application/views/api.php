
<script>
    var marker1=null;
    var marker2=null;
    function initialize() {

        map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

        google.maps.event.addListener(map, "click", function (e) {
            console.log("done");
            var latLng = e.latLng;

            if(marker1==null){
                document.getElementById("latitude1").value=latLng.lat();
                document.getElementById("longitude1").value=latLng.lng();


                var pinColor = "CC00FF";
                var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                    new google.maps.Size(21, 34),
                    new google.maps.Point(0,0),
                    new google.maps.Point(10, 34));

                marker1=new google.maps.Marker({
                    position:latLng,
                    draggable:true,
                    icon:pinImage
                });

                marker1.setMap(map);

                google.maps.event.addListener(marker1, "drag", function (e) {
                    var latLng=e.latLng;
                    document.getElementById("latitude1").value=latLng.lat();
                    document.getElementById("longitude1").value=latLng.lng();

                });
            }else{
                document.getElementById("latitude2").value=latLng.lat();
                document.getElementById("longitude2").value=latLng.lng();

                if(marker2!=null)return;

                var pinColor = "FE7569";
                var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                    new google.maps.Size(21, 34),
                    new google.maps.Point(0,0),
                    new google.maps.Point(10, 34));

                marker2=new google.maps.Marker({
                    position:latLng,
                    draggable:true,
                    icon:pinImage
                });

                marker2.setMap(map);

                google.maps.event.addListener(marker2, "drag", function (e) {
                    var latLng=e.latLng;
                    document.getElementById("latitude2").value=latLng.lat();
                    document.getElementById("longitude2").value=latLng.lng();

                });
            }


        });


    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<table>
    <tr>
        <td>

            <?php
            echo heading("GET devAPI",3);
            echo validation_errors();
            if(isset($success)){echo heading($success,4).br();}
            //page/add_location



            echo form_input(array(
                "type"=>"float",
                "placeholder"=>"Latitude1",
                "name"=>"latitude1",
                "required"=>"true",
                "id"=>"latitude1",
            ));

            echo form_input(array(
                "type"=>"float",
                "placeholder"=>"Longitude1",
                "name"=>"longitude1",
                "required"=>"true",
                "id"=>"longitude1",
            )).br(2);

            echo form_input(array(
                "type"=>"float",
                "placeholder"=>"Latitude2",
                "name"=>"latitude2",
                "required"=>"true",
                "id"=>"latitude2",
            ));

            echo form_input(array(
                "type"=>"float",
                "placeholder"=>"Longitude2",
                "name"=>"longitude2",
                "required"=>"true",
                "id"=>"longitude2",
            ));
            ?>
            <br /><br />
            Host: <br />
            <?php echo form_input(array(
                    "type"=>"text",
                    "id"=>"host",
                    "required"=>"true",
                    "value"=>site_url("api"))).br(2);
            ?>
            Token: <br />
            <?php echo form_input(array(
                "type"=>"text",
                "id"=>"token",
                "required"=>"true",
                "value"=>"NCfkCU1BAaIOGu7lJ0cpeZes7c3buiwhUMCxHD8iOQ6vldQ5d2")).br(2);
            ?>


            <button onClick="call()" class="button btn btn-small btn-primary">Go!</button>

        </td>
        <td>
            <div id="googleMap" style="width:800px;height:580px;"></div>
        </td>
    </tr>
</table>



<script>
    function call(){
        var host=$("#host").val();

        if(!host){ alert("host not entered"); return;}
        var fLat=$("#latitude1").val();
        var fLng=$("#longitude1").val();

        var tLat=$("#latitude2").val();
        var tLng=$("#longitude2").val();

        var token=$("#token").val();

        var req=host+"/?fromLat="+fLat+"&fromLng="+fLng+"&toLat="+tLat+"&toLng="+tLng+"&token="+token;

        window.open(req);
    }


</script>
