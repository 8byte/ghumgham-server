<?php
class Get extends CI_Controller
{

    function __construct($res=NULL){
        parent::__construct();
        $this->load->helper("loader");
    }

    function locationsSelect(){
        echo form_dropdown("locations",dropdown_array_of_locations($this->locationHandler->getAllLocations()),"",'  class="form-control conns"');
    }

    function linksOf($place){
        $place=$place+0;
        if(!exists_location($place))echo "";
        else{
            $places=$this->roadwayHandler->getAllPlacesConnectedTo($place);
        }
        //$result=array("locations"=>$places);
        echo json_encode($places,JSON_PRETTY_PRINT);
    }

    function vehicleDescription($id){
        if(!exists_vehicle($id)) die("no such vehicle exists.");
        else{
            $vehicle=$this->vehicleHandler->getVehicleByIdInProperTable($id,$mode="array");
            echo json_encode($vehicle, JSON_PRETTY_PRINT);
        }
    }

    function multiverse(){
        $places=$this->popularPlaceHandler->getAll();

        $cats=array();
        foreach($places as $place){
            $found=false;

            for($i=0;$i<count($cats);$i++){
                $cat=&$cats[$i];
//                var_dump($cat);

                if($place->category == $cat['name']){
//                    array_push($cat['data'],formatPopularPlaceForJson($place));

                    $found=true;
                    break;
                }

            }
            if(! $found){

                array_push($cats,array(
                    "name"=>$place->category,
//                    "data"=>array(formatPopularPlaceForJson($place)),
                    "icon"=>"",
                ));
            }
        }

        for($i=0;$i<count($cats);$i++){
            $cats[$i]['icon']= base_url("resources/img/icons/".strtolower($cats[$i]['name']).".png");
            $cats[$i]['universe']= site_url("get/universe/".strtolower($cats[$i]['name'])."");
        }
        header("Content-Type:application/json");
        echo json_encode($cats,JSON_PRETTY_PRINT);
    }

    function universe($cat=""){
        if(! $cat){
            return json_encode(array(
                "status"=>1,
                "message"=>"404 not found",
            ),JSON_PRETTY_PRINT);
        }

        $places=$places=$this->popularPlaceHandler->ofCat($cat);


        foreach($places as &$place){
            $place=formatPopularPlaceForJson($place);
        }

        echo json_encode($places,JSON_PRETTY_PRINT);


    }



}
?>
