<?php
function loggedIn(){
	$res=&get_instance();
	if($res->session->userdata("logged_in_user")){
		return true;
	}
	return false;
}
function logOut(){
	$res=&get_instance();
	$res->session->unset_userdata("logged_in_user");
	redirect("login");
}
function matchesAdmin($user,$pass){
	$res=&get_instance();
	if ($res->config->config['username']==$user and $res->config->config['password']==$pass){
		return true;
	}
	return false;
}
function loginUser($username){
	$res=&get_instance();
	$res->session->set_userdata(array("logged_in_user"=>$username));
	redirect("page");
}
function ensure_login(){
	$res=&get_instance();
	if(!loggedIn($res)){
		redirect("login");
	}
}
?>