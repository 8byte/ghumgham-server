
<?php
echo heading("All Locations",3);
echo validation_errors();
if(isset($success)) echo "<h3>$success</h3>";
?>

<script>
function confirmDeletion(vID){
	if(confirm("Do you want to delete?")){
		window.open("<?php echo site_url("delete/popular_place/");?>/"+vID);
		
		var element = document.getElementById("tRow"+vID);
		element.parentNode.removeChild(element);
	}
}
</script>

<table border=1 class="table table-hover table-bordered">
<tr>
<th>ID</th><th>Name</th><th>Latitude</th><th>Longitude</th><th>District</th></tr>


<?php
if(!isset($array)) die("array not set");

for($i=0;$i<sizeof($array);$i++){
	echo "<tr id='tRow".$array[$i]->ID."'>\n";
	echo form_open("show/popular_place");
	
	echo "<td>".$array[$i]->ID.form_input(array("type"=>"hidden","name"=>"ID","value"=>$array[$i]->ID))."</td>\n";
	echo "<td>".form_input(array("type"=>"text","name"=>"name","value"=>$array[$i]->name,"class"=>"form-control"))."</td>\n";
	echo "<td>".form_input(array("name"=>"latitude","value"=>$array[$i]->latitude,"class"=>"form-control"))."</td>\n";
	echo "<td>".form_input(array("name"=>"longitude","value"=>$array[$i]->longitude,"class"=>"form-control"))."</td>\n";
	echo "<td>".form_dropdown("category",$res->popularCategories,$array[$i]->category,'class="form-control"')."</td>\n";
	echo "<td>".form_submit("","Update")."</td>";
	echo "<td><a href='javascript:confirmDeletion(".$array[$i]->ID.")' url='".site_url("delete/popular_place/".$array[$i]->ID)."' >Delete</a>"."</td>";
	echo "\n</form>\n";
	echo "</tr>\n";
	
}
?>
</table>
