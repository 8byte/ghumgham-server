<?php 
function latLngDiffInKm($latDiff,$lngDiff){
	static $lat_into_km_factor=50;
	static $lng_into_km_factor=50;
	
	$distance=pythogorasTheorem($latDiff*$lat_into_km_factor, $lng_into_km_factor*$lngDiff);
	return $distance;
}
?>