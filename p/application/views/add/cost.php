<?php

echo heading("Add cost",3);
echo validation_errors();

if(isset($success)){echo heading($success,4).br();}

echo form_open("page/add_cost");
echo "Place 1:".form_dropdown("place1",dropdown_array_of_locations($res->locationHandler->getAllLocations()),'',"id='a1' class='form-control'").br();
echo "Place 2:".form_dropdown("place2",dropdown_array_of_locations($res->locationHandler->getAllLocations()),'',"id='a1' class='form-control'").br();
echo "Cost: ".form_input(
	array("name"=>"cost",
		"type"=>"number",
        "class"=>"form-control"
    )
	).br();

echo "Vehicle type: ".form_dropdown("vType",dropdown_array_of_vehicleTypes(),"",'class="form-control"').br();
echo form_submit(array("value"=>"Submit form","class"=>"form-control")).br();
echo "</form>";

?>