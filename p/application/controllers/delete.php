<?php
class Delete extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->helper("loader");
		ensure_login();
	}

	function location($id=0){
	
		$id=$id+0;
		if(exists_location($id)){
			echo $this->locationHandler->deleteLocation(array("ID"=>$id));
			echo "Deleted location with ID $id";
		}else{
			echo "invalid ID:$id";
		}
	}


	function cost($id=0){
	
		$id=$id+0;
		if(exists_cost($id)){
			echo $this->costHandler->delete(array("ID"=>$id));
			echo "Deleted cost with ID $id";
		}else{
			echo "invalid ID:$id";
		}
	}
	
	function vehicle($id=0){
		$id=$this->input->get_post("id")+0;
		if(exists_vehicle($id)){
			$this->vehicleHandler->deleteVehicle($id);
			echo "Deleted vehicle with ID $id";
		}else{
			echo "invalid ID:$id";
		}
	}	
	
	function route($id=0){
		$id=$id+0;
		if(exists_route($id)){
			
			echo $this->routeHandler->deleteRouteAndAssociations($id);
			
			echo br()."Deleted location with ID $id";
		}else{
			echo "invalid ID:$id";
		}
	}

	function popular_place($id=0){
		$id=$id+0;
		if(exists_popular_place($id)){
			
			echo $this->popularPlaceHandler->delete($id);
			
			echo br()."Deleted popularPlace with ID $id";
		}else{
			echo "invalid ID:$id";
		}
	}
	function roadway($id=0){
		$id=$id+0;
		
		if(exists_roadway($id)){
			$this->roadwayHandler->deleteRoadway($id);
			echo "roadway $id found and deleted";
		}else{
			echo "roadway not found";
		}
	}
}
?>