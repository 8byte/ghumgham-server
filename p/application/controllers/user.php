<?php
/**
 * Created by PhpStorm.
 * User: damo
 * Date: 12/13/15
 * Time: 8:43 PM
 */


class user extends CI_Controller{
    function __construct(){
        parent::__construct();

        $this->load->helper("loader");
    }

    function login($key=""){
        $code="ya29.SgJ809ahgRG6meI-ZQLQ6bDSCt8AcqKGfDao3uBlNEtdn9xWdo08p5hRnWAb9xzvjr8C";
        $code=$key;

        $ret=array();
        try{
            $url= "https://www.googleapis.com/plus/v1/people/me?access_token=$code";

            $json = @file_get_contents($url);
            if($json==FALSE) throw new Exception("Could not fetch");

            $obj = json_decode($json);

            $isForeign=true;
            foreach($obj->placesLived as $place){
                if(strtolower($place->value)=="kathmandu" && $place->primary==true){
                    $isForeign=false;
                    break;
                }
            }

            $email="";
            foreach($obj->emails as $em){
                $email=$em->value;
            }

            $customToken=generateRandomString(50);
            $parsed=array(
                "name"=>$obj->displayName,
                "isForeign"=>$isForeign,
                "email"=>$email,
                "Gid"=>$obj->id,
                "token"=>$customToken,
            );

            $this->userHandler->insertOrUpdate($parsed);

            $ret['status']=0;
            $ret['message']="Successfully logged in";
            $ret['name']=$obj->displayName;
            $ret['token']=$parsed['token'];
            $ret['isForeign']=$parsed['isForeign'];

//            $ret['url']=$url;
        }catch (Exception $e){
            $ret['status']=1;
            $ret['message']=$e->getMessage();

        }

        echo json_encode($ret);
    }

    function api(){
        $ret=array("status"=>0,"message"=>"Success","data"=>[]);

        $key=$this->input->get_post("token","");

        $tokenValid=$this->userHandler->isTokenValid($key);

        if($tokenValid){
            $count=$this->userHandler->getApiFetchCountForToken($key);

            $canFetchApi=$this->userHandler->canFetchApi($key);

            if($canFetchApi){
                $ret['count']= ++$count;
                $this->userHandler->setApiFetchCountForToken($key,$count);

                $fromLat=$this->input->get("fromLat",27.608884294366522);
                $fromLng=$this->input->get("fromLng",85.36307215690613);
                $toLat=$this->input->get("toLat",27.693560337227815);
                $toLng=$this->input->get("toLng",85.31982421875);

                $host=$this->config->config['api_host'];

                $url=$host."?fromLat=$fromLat&toLat=$toLat&fromLng=$fromLng&toLng=$toLng";

                $result=json_decode(@file_get_contents($url));

                if(! $result){
                    $ret['status']=2;
                    $ret['message']="Server down for maintenance";
                }else{
                    $ret['message']="Fetched!";
                    $ret['url']=$url;
                    $ret['data']=$result;
                }



            }else{
                $ret['status']=1;
                $ret['message']="Please buy the app for more";
            }
        }

        else{
            $ret['status']=4;
            $ret['message']="Use a valid token";


        }


        echo json_encode($ret,JSON_PRETTY_PRINT);
    }

} 