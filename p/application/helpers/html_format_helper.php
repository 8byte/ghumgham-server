<?php

function dropdown_array_of_locations($array){
	$ret=array();
	foreach ($array as $item){
		$str=format_location($item);
		$ret[$item->ID]=$str;
	}
	return $ret;
}
function is_bus_stop_form_field($name,$defaultValue=""){
	return form_dropdown($name,array("1"=>"Is bus stop", "0"=>"Can not stop here"),$defaultValue,"class='form-control'");
}

function dropdown_array_of_vehicleTypes(){
	$vehicleTypes=array();
	$list2=get_instance()->vehicleTypeHandler->getAllTypes();
	foreach($list2 as $item){
		$vehicleTypes[$item->ID]=format_vehicleType($item);
	}
	return $vehicleTypes;
}

function dropdown_array_of_routes($routes){
	$ret=array();
	$CI=&get_instance();
	foreach($routes as $item){
		$str=format_route($item);
		$ret[$item->ID]=$str;
	}
	return $ret;
}

function dropdown_array_of_roadways($array){
	$ret=array();
	$CI=&get_instance();
	foreach($array as $item){
		$str=format_location($CI->locationHandler->getLocationByID($item->point1))." - ".format_location($CI->locationHandler->getLocationByID($item->point2));
		$ret[$item->ID]=$str;
	}
	return $ret;
}
function php_dropdown_to_js_text($dropdown,$name){
	//convert <select> to appear as var js='text'
	$str="<select name='$name'>";
	foreach($dropdown as $key=>$item){
		$str.= "<option value='$key'>$item</option>";
	}
	$str.="</select>";
	return $str;
}

function php_dropdown_to_js_eval($dropdown,$name,$selectElement=""){
	//convert <select> to appear as var js='text'
	if($selectElement==""){
		$str="var select=document.createElement('select');\n";//<select name='$name'>";
	}else{
		$str="var select=document.getElementById('".$selectElement."')";
	}
	$str="";
	foreach($dropdown as $key=>$item){
		$str .="var option".$key."=document.createElement('option');\n";
		$str .="option".$key.".value='".json_encode($key)."'\n";
		$str .="option".$key.".innerHTML=".json_encode($item)."\n";
		$str .="select.appendChild(option".$key.")\n";
		//$str.= "<option value='$key'>$item</option>";
	}
	//$str.="</select>";
	return $str;
}

function createGoogleMapMarkersFor($array,$map="map"){
	//creates markers for google map loctions from $array
	echo "var latLng;\n";
	echo "var marker;\n";
	foreach($array as $location){
		//print_r($location);
		echo "latLng = new google.maps.LatLng(".$location->latitude.", ".$location->longitude.");

		marker=new google.maps.Marker({
		  position:latLng,
		  });

		marker.setMap($map);
		";
	}
	
}
function php_array_to_js_array($array){
	$str= "[";
	$count=0;
	foreach ($array as $key=>$value){
		$count++;
		$str.= '{"key":"'.htmlspecialchars($key).'","value":"'.htmlspecialchars($value).'"}';
		if($count<sizeof($array)) $str.=",";
	};
	$str.=" ];";
	return $str;
}

function render_table($table){
	echo "<table border=1 class='table'>";
	echo "<tr>";
	echo "<th colspan=".sizeof($table['columns']).">";
	echo $table['title'];
	echo "</th>";
	echo "</tr>";
	
	echo "<tr>";
	foreach($table['columns'] as $column){
		echo "<th>".$column."</th>";
	}
	echo "</tr>";
	
	foreach($table['rows'] as $row){
		echo "<tr>";
		foreach($table['columns'] as $column){
			echo "<td>".$row[$column]."</td>";
		}
		echo "</tr>";
	}
	echo "</table>".br();
}

function xml_convert_location($location){
	$ID=0;
	$name="";
	$lat=0;
	$lon=0;
	$district="";
	if(isset($location->ID)){
		//$location is an object
		$ID=$location->ID;
		$name=$location->name;
		$lat=$location->latitude+0;
		$lon=$location->longitude+0;
		$district=$location->district;
		
	}else{//otherwise it is supposed to be an array
		$ID=$location['ID'];
		$name=$location['name'];
		$lat=$location['latitude']+0;
		$lon=$location['longitude']+0;
		$district=$location['district'];
	}
	return "<location>
		<ID>".$ID."</ID>
		<name>".$name."</name>
		<latitude>".$lat."</latitude>
		<longitude>".$lon."</longitude>
		<district>".$district."</district>
</location>";
}

function xml_convert_locations($locations){
	$str="<locations>\n";
		foreach($locations as $place){
			$str .=xml_convert_location($place)."\n";
		}
		$str .="</locations>";
		return $str;
}
?>
