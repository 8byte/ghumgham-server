<?php
class Show extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->helper("loader");
		ensure_login();
	}
	
	function roadway(){
		$params=array("title"=>"Show roadways");
		
		$roadways=$this->roadwayHandler->getAllRoadways($mode="object");
		
		$table=array("title"=>"All roadways",'rows'=>array());
		$table['columns']=array("ID","Begin place","end place","split","edit","delete");
		
		foreach($roadways as $roadway){
			$myArray=array();
			$myArray['ID']=$roadway->ID;
//            var_dump($roadway->point1);
			$myArray['place1']=format_location($this->locationHandler->getLocationById($roadway->point1));
			$myArray['place2']=format_location($this->locationHandler->getLocationById($roadway->point2));
			array_push($table['rows'],$myArray);
		}
		$params['array']=$table;
		
		load_page("show/roadway",$params);
	}

	function cost(){
		$params=array("title"=>"Show costs");		
		
		$this->form_validation->set_rules("place1","Place 1","required|is_natural_no_zero|exists_location");
		$this->form_validation->set_rules("place2","Place 2","required|is_natural_no_zero|exists_location");
		$this->form_validation->set_rules("cost","cost","required|is_natural_no_zero|less_than[100]");
		$this->form_validation->set_rules("vType","vType","required|exists_vehicleType");
		$this->form_validation->set_rules("ID","cost ID","required|is_natural_no_zero|exists_cost");
		
		//print_r($_POST);
		if($this->form_validation->run()){

			$p1=set_value("place1")+0;
			$p2=set_value("place2")+0;
			$cost=set_value("cost")+0;
			$vType=set_value("vType")+0;
			
			$id=set_value("ID");

			if($p1==$p2){
				$params['success']="Place1 and Place2 must be different";
			}else{
				$this->costHandler->update($id,array("place1"=>$p1,
												"place2"=>$p2,
												"vehicleType"=>$vType,
												"cost"=>$cost));

				$params['success']="Cost updated!";
			}
		}
		$costs=$this->costHandler->getAll();
		$params['array']=$costs;
		
		load_page("show/cost",$params);
	}

	function location(){
		$params=array("title"=>"Show locations");		
		
		$this->form_validation->set_rules("latitude","Latitude","required|decimal|greater_than[-1]");
		$this->form_validation->set_rules("longitude","Longitude","required|decimal|greater_than[-1]");
		$this->form_validation->set_rules("name","PlaceName","required|xss_clean|max_width[".$this->config->config['db']['location']['name']['length']."]");
		$this->form_validation->set_rules("ID","location ID","required|is_natural_no_zero|exists_location");
		$this->form_validation->set_rules("isStop","Bus Stop","required|is_natural|less_than[2]");
		
		//print_r($_POST);
		if($this->form_validation->run()){
			$id=set_value("ID");
			$name=set_value("name");
			$latitude=set_value("latitude")+0;
			$longitude=set_value("longitude")+0;
			$isStop=(set_value("isStop")+0)>0;

			$this->locationHandler->update($id,array("name"=>$name,
												"latitude"=>$latitude,
												"isStop"=>$isStop,
												"longitude"=>$longitude));
			$params['success']=$name." updated";
			
		}
		$locations=$this->locationHandler->getAllLocations($sort=true);
		$params['array']=$locations;
		
		load_page("show/location",$params);
	}

	function popular_place(){
		$params=array("title"=>"Show Popular Places");		
		
		$this->form_validation->set_rules("latitude","Latitude","required|decimal|greater_than[-1]");
		$this->form_validation->set_rules("longitude","Longitude","required|decimal|greater_than[-1]");
		$this->form_validation->set_rules("name","PlaceName","required|xss_clean|max_width[100]");
		$this->form_validation->set_rules("category","Category","required|xss_clean|max_width[30]");
		$this->form_validation->set_rules("ID","location ID","required|is_natural_no_zero|exists_location");
		
		
		if($this->form_validation->run()){
			$id=set_value("ID");
			$name=set_value("name");
			$latitude=set_value("latitude")+0;
			$longitude=set_value("longitude")+0;
			$category=set_value("category");

			
			$data=array("ID"=>$id,
						"name"=>$name,
						"category"=>$category,
						"latitude"=>$latitude,
						"longitude"=>$longitude);

			$this->popularPlaceHandler->update($id,$data);
			$params['success']=$name." updated";
			
		}
		$locations=$this->popularPlaceHandler->getAll();
		$params['array']=$locations;
		
		load_page("show/popular_place",$params);
	}

	function vehicle(){
		$params=array("title"=>"Show vehicles");
	
		$this->form_validation->set_rules("ID","vehicle ID","required|is_natural_no_zero|exists_vehicle");
		$this->form_validation->set_rules("vType","vehicle Type","required|is_natural_no_zero|exists_vehicleType");
		$this->form_validation->set_rules("vRoute","Route","required|is_natural_no_zero|exists_route");
        $this->form_validation->set_rules("desc","Vehicle desc","");
		$this->form_validation->set_rules("name","Vehicle name","required|max_len[".$this->config->config['db']['vehicle']['name']['length']."]|xss_clean");

		if($this->form_validation->run()){
			//remove the old vehicle
			
			$id=set_value("ID");
			$name=set_value("name");
			$routeID=set_value("vRoute");
			$vTypeID=set_value("vType");
            $desc=set_value("desc");
			
			$this->vehicleHandler->deleteVehicle($id);
			$this->vehicleHandler->addVehicle(array("ID"=>$id,"name"=>$name,"desc"=>$desc,
						"routeID"=>$routeID,"typeID"=>$vTypeID));
			$params['success']=$name." updated!";
			//add the new vehicle with params
		}
		$vehicles=$this->vehicleHandler->getAllVehicles();
		$params['array']=$vehicles;
		$params['vehicleTypes']=$this->vehicleHandler->getAllVehicleTypes();
		$params['routes']=$this->routeHandler->getAllRoutesInProperFormat();
		
		load_page("show/vehicle",$params);
	}
	
	function routeLocation(){
		$params=array("title"=>"Show route locations");
		$params['loadGoogleMapAPI']=TRUE;
		//$this->form_validation->set_rules("id","Route","required|is_natural_no_zero|exists_route");
		$routes=$this->routeHandler->getAllRoutes();
		
		$params['route']=$params['routes']=dropdown_array_of_routes($routes);
		$route=$this->input->get_post("id")+0;
		
		if(exists_route($route)){
						
			$locations=$this->routeLocationHandler->getRouteLocationsOfRoute($route);

			
			$routeName=$this->routeHandler->getRouteByID($route)->remarks;
			$params['routeName']=$routeName;
			
			$table=array("title"=>"Locations lying in route",'rows'=>array());
			$table['columns']=array("place");
			foreach($locations as $location){
				array_push($table['rows'],array("place"=>format_location($location)));
			}
			
			$params['table']=$table;
			$params['routeID']=$route;
			$params['locations']=$locations;
			$params['GoogleMapsData']=array();
			
			$vehicles=array("title"=>"Vehicles lying in route",'rows'=>array());
			$vehicles['columns']=array("vehicle","delete");
			$vehiclesLying=$this->vehicleHandler->getVehiclesLyingInRoute($route);
			foreach($vehiclesLying as $dbVehicle){
				$vText=format_vehicle($dbVehicle);
				$delText=anchor("delete/vehicle?id=".$dbVehicle->ID,"Delete");
				array_push($vehicles['rows'],array("vehicle"=>$vText,"delete"=>$delText));
			}
			
			$params['vehicleTable']=$vehicles;
		}else{
			print_r($_REQUEST);
		}
		load_page("show/routeLocation",$params);
	}
}
