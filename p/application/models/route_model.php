<?php
class Route_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function addRoute($data){
		return $this->db->insert("route",$data);
	}
	function addRouteAndReturnID($data){
		$this->db->insert("route",$data);
		return $this->db->insert_id();
	}
	function getAllRoutes(){
		return $this->db->get("route")->result();
	}
	function deleteRouteAndAssociations($id){
		$inst=&get_instance();
		
		//delete all roadways associated
		$inst->routeLocationHandler->deleteLocationsOfRoute($id);

		//delete the route itself
		return $inst->routeHandler->deleteRoute(array("ID"=>$id));}
	
	function getSingleRouteInProperFormat($id){
		$result=$this->db->get_where("route",array("ID"=>$id))->result_array();
		
		$ret=array();
		foreach ($result as $item){
			$ret[$item['ID']]=$item['remarks'];
		}
		return $ret;
	}
	function getAllRoutesInProperFormat(){
		$result=$this->db->get("route")->result_array();
		
		$ret=array();
		foreach ($result as $item){
			$ret[$item['ID']]=$item['remarks'];
		}
		return $ret;
	}
	function deleteRoute($data){
		return $this->db->delete("route",$data);
	}
	function getRouteByID($id){
		return $this->db->get_where("route",array("ID"=>$id))->result()[0];
	}
	function existsRoute($id){
		return (sizeof($this->db->get_where("route",array("ID"=>$id))->result())>0);
	}
}
?>