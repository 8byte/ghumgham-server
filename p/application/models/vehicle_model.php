<?php
class Vehicle_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function addVehicle($data){
		return $this->db->insert("vehicle",$data);
	}
	function getAllVehicles(){
		return $this->db->get("vehicle")->result();
	}
	function existsVehicle($id){
		$this->db->where("ID",$id);
		return (sizeof($this->db->get("vehicle")->result())>0);
	}
	function deleteVehicle($id){
		return $this->db->delete("vehicle",array("ID"=>$id));
	}
	
	function getAllVehicleTypes(){
		$result=$this->db->get("vehicletype")->result_array();
		
		$ret=array();
		foreach ($result as $item){
			$ret[$item['ID']]=$item['name'];
		}
		return $ret;
	}
	function getVehiclesLyingInRoute($id){
		$this->db->where("routeID",$id);
		return $this->db->get("vehicle")->result();
	}
	
	function getAllVehiclesInProperTable(){
		//merge tables from database and return the array
		$query="SELECT route.ID as routeID, route.remarks as routeName, vehicle.ID as vehicleID,vehicle.name as vehicleName,vehicle.typeID as typeID, vehicletype.name as typeName from (vehicle inner join vehicletype on vehicle.typeID=vehicletype.ID) inner join route on vehicle.routeID=route.ID;";
		$result=$this->db->query($query);
		return $result->result();
	}
	
	function getVehicleByIdInProperTable($id,$mode="array"){
		//merge tables from database and return the array
		$query="SELECT route.ID as routeID, route.remarks as routeName, vehicle.ID as vehicleID,vehicle.name as vehicleName,vehicle.typeID as typeID, vehicletype.name as typeName from (vehicle inner join vehicletype on vehicle.typeID=vehicletype.ID) inner join route on vehicle.routeID=route.ID where vehicle.ID=$id";
		$result=$this->db->query($query);
		if($mode=="array")
			return $result->result_array()[0];
		else
			return $result->result()[0];
	}
}
?>