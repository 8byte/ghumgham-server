<?php
echo heading("Add Vehicle",3);
echo validation_errors();
if(isset($success)){echo heading($success,4).br();}
echo form_open("page/add_vehicle");

echo "Vehicle name: ".form_input(array("type"=>"text","name"=>"name","class"=>"form-control")).br();
echo "Vehicle desc: ".form_input(array("type"=>"text","name"=>"desc","class"=>"form-control")).br();
echo "Route: ".form_dropdown("routeID",$routes,"","class='form-control'").br();
echo "VehicleType: ".form_dropdown("vehicleTypeID",dropdown_array_of_vehicleTypes(),"",'class="form-control"');

echo form_submit(array("value"=>"Submit form","class"=>"form-control")).br();
echo "</form>";
?>