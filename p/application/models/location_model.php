<?php
class Location_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function addLocation($data){
		return $this->db->insert("location",$data);
	}
    function addLocationAndReturnID($data){
        $this->addLocation($data);

        return $this->db->insert_id();
    }
	function deleteLocation($data){
		return $this->db->delete("location",$data);
	}
	function getAllLocations($sort=true){
		if($sort==true){
			return $this->db->get("location")->result();
		}else
			return $this->db->get("location")->result();
		
	}

	function update($id,$data){
		$this->db->where("ID",$id);
		$this->db->update("location",$data);
	}
	function getDistanceBetween($point1,$point2){
		//returns the Pythagorean difference of latitude and longitude between two points
		
		$point1=$this->getLocationByID($point1);
		$point2=$this->getLocationByID($point2);
		
		$latDiff=$point1->latitude-$point2->latitude;
		$lngDiff=$point1->longitude-$point2->longitude;
		$distance=sqrt($latDiff*$latDiff+$lngDiff*$lngDiff);
		return $distance;
	}
	function getLocationByID($id,$mode="object"){
		//$mode determines whether the return object is object or array
		$this->db->where("ID",$id);
		$res=$this->db->get("location");
		if($res->num_rows()){
			if($mode=="object")
				$res=$res->result();
			else
				$res=$res->result_array();
			return $res[0];
		}
		return NULL;
	}
	
	
	function existsLocation($id){
		$this->db->where("ID",$id);
		return ($this->db->get("location")->num_rows()>0);
	}
	function existsLocations($array){
		foreach ($array as $arrayItem){
			if (!($this->existsLocation($arrayItem))) return false;
		}
		return true;
		$sql="SELECT COUNT(*) as count from location where ID in (".implode(",",$array).")";
		$result=$this->db->query($sql)->result_array();
		return ($result[0]['count']==sizeof($array));
	}
	function getNearestPlacesFrom($lat,$lon,$limit=3){
		$this->db->order_by("((latitude-$lat)*(latitude-$lat)+(longitude-$lon)*(longitude-$lon))");
		$this->db->limit($limit);
		return $this->db->get("location")->result();
	}
}

?>