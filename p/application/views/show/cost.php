
<?php
echo heading("All Costs",3);
echo validation_errors();
if(isset($success)) echo "<h3>$success</h3>";
?>

<script>
function confirmDeletion(vID){
	if(confirm("Do you want to delete?")){
		window.open("<?php echo site_url("delete/cost/");?>/"+vID);
		
		var element = document.getElementById("tRow"+vID);
		element.parentNode.removeChild(element);
	}
}
</script>

<table border=1 class="table table-bordered table-hover">
<tr>
<th>ID</th><th>Place1</th><th>Place2</th><th>Cost</th></tr>


<?php
if(!isset($array)) die("array not set");

$allLocations=$res->locationHandler->getAllLocations();
for($i=0;$i<sizeof($array);$i++){
	echo "<tr id='tRow".$array[$i]->ID."'>\n";
	echo form_open("show/cost");
	
	echo "<td>".$array[$i]->ID.form_input(array("type"=>"hidden","name"=>"ID","value"=>$array[$i]->ID))."</td>\n";
	echo "<td>".form_dropdown("place1",dropdown_array_of_locations($allLocations),$array[$i]->place1,'class="form-control"')."</td>";


	echo "<td>".form_dropdown("place2",dropdown_array_of_locations($allLocations),$array[$i]->place2,"class='form-control'")."</td>";

	echo "<td>".form_input(array("name"=>"cost","type"=>"number","value"=>$array[$i]->cost,"class"=>"form-control"))."</td>\n";
	echo "<td>".form_dropdown("vType",dropdown_array_of_vehicleTypes(),$array[$i]->vehicleType,'class="form-control"')."</td>";

	echo "<td>".form_submit(array("value"=>"Update","class"=>"form-control"))."</td>";
	echo "<td><a href='javascript:confirmDeletion(".$array[$i]->ID.")' url='".site_url("delete/cost/".$array[$i]->ID)."' >Delete</a>"."</td>";
	echo "\n</form>\n";
	echo "</tr>\n";
	
}
?>
</table>
