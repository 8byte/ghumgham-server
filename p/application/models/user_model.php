<?php
class User_model extends CI_Model{
    function __construct(){
        parent::__construct();

    }
    function add($data){
        $this->db->insert("user",$data);
        return $this->db->insert_id();
    }
    function delete($id){
        $this->db->where("Gid",$id);
        return $this->db->delete("user");
    }
    function getAll(){
        return $this->db->get("user")->result();
    }

    function exists($id){
        $this->db->where("Gid",$id);
        return ($this->db->get("user")->num_rows()>0);
    }
    function update($id,$data){
        $this->db->where("Gid",$id);
        return $this->db->update("user",$data);
    }

    function insertOrUpdate($data){
        $exists=$this->exists($data['Gid']);

        if($exists){
            $this->update($data['Gid'],$data);
        }else{
            $this->add($data);
        }
    }

    function isTokenValid($key){

        $this->db->where("token",$key);
//        echo $key;
        return ($this->db->get("user")->num_rows()>0);
    }

    function getApiFetchCountForToken($token){
        $this->db->where("token",$token);

        assert($this->isTokenValid($token));

        return $this->db->get("user")->result()[0]->apiFetchCount+0;;
    }
    function setApiFetchCountForToken($token,$count){
        $this->db->where("token",$token);

        $this->db->update("user",array("apiFetchCount"=>$count));
    }

    function userWithTokenIsForeign($token){
        assert($this->isTokenValid($token));

        return ($this->db->get_where("user",array("isForeign"=>TRUE))->num_rows()>0);
    }
    function canFetchApi($token){
        assert($this->isTokenValid($token));

        $foreign=$this->userWithTokenIsForeign($token);
        if(! $foreign){
            return true;
        }

        $count=$this->getApiFetchCountForToken($token);
        if($count<get_instance()->config->config['demoFetchLimit']){
            return true;
        }
    }
}

?>