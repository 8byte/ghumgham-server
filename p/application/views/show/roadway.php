<?php
echo heading("All Roadways",3);
if(isset($success)) echo "<h3>$success</h3>";
?>


<script>

function confirmdeletion(vID){
	if(confirm("Do you want to delete?")){
		window.open("<?php echo site_url("delete/roadway/");?>/"+vID);
		
		var element = document.getElementById("tRow"+vID);
		element.parentNode.removeChild(element);
	}else{
	}
}
</script>

<table border=5 class="table table-hover">
<tr>
<!--
<th>ID</th>
-->
<th>Begin place</th><th>End place</th><th>Break</th><th>Delete</th></tr>


<?php
if(!isset($array)) die("array not set");

for($i=0;$i<sizeof($array['rows']);$i++){
	echo "<tr id='tRow".$array['rows'][$i]['ID']."'>\n";	
	//echo "<td>".$array['rows'][$i]['ID']."</td>\n";
	echo "<td>".$array['rows'][$i]['place1']."</td>";
	echo "<td>".$array['rows'][$i]['place2']."</td>";
	echo "<td>".anchor("page/split_roadway/".$array['rows'][$i]['ID'],"Split")."</td>";
	echo "<td><a href='javascript:confirmdeletion(\"".$array['rows'][$i]['ID']."\")' url='".site_url("delete/roadway/".$array['rows'][$i]['ID'])."' >Delete</a>"."</td>";
	echo "\n</form>\n";
	echo "</tr>\n";
	
}
?>
</table>
