<?php
class Popular_Place_Image_model extends CI_Model{
	function __construct(){
		parent::__construct();
		
	}
	function add($data){
		$this->db->insert("popular_place_image",$data);
        return $this->db->insert_id();
	}
	function delete($id){
		$this->db->where("ID",$id);
		return $this->db->delete("popular_place_image");
	}
	function getAll(){
		return $this->db->get("popular_place_image")->result();
	}
	
	function exists($id){
		$this->db->where("ID",$id);
		return ($this->db->get("popular_place_image")->num_rows()>0);
	}
	function update($id,$data){
		$this->db->where("ID",$id);
		return $this->db->update("popular_place_image",$data);
	}
    function getOfPlace($id){
        $this->db->where("popular_place_id",$id);
        return ($this->db->get("popular_place_image")->result());
    }
}

?>