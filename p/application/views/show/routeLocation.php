<?php
	if(isset($routeName))echo heading($routeName,3);

//var_dump($table);
	if(isset($table)){
		echo "<table class=table><tr><td>";
		render_table($table);
		echo "</td>";
		if(isset($GoogleMapsData)){
			echo "<td>";?>
			
<script>
function initialize() {
  map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
  
  //load all data from the first dropdown
  <?php
  echo "var latLng;\n";
	echo "markers=[];\n";
	echo "polyLines=[];\n";
	echo "markers['index1']={};\n";
	$initialLocation=NULL;
	for($i=0;$i<sizeof($locations);$i++){
		$location=$locations[$i];
		$id=$location->ID;
		$markerName="markers['index1']['id".$id."']";
		
		//echo "var $markerName;\n";
		
		//print_r($location);
		echo "latLng = new google.maps.LatLng(".$location->latitude.", ".$location->longitude.");

		$markerName=new google.maps.Marker({
		  position:latLng,
		  RoadwayLocationID:".$location->ID.",
		  RoadwayLocationIndex:1
		  });
		 
		$markerName.setMap(map);
		
		";
		//draw connector line
		if($i>0){//not first item
			echo "var endPos = new google.maps.LatLng(".$location->latitude.", ".$location->longitude.");
			var beginPos = new google.maps.LatLng(".$initialLocation->latitude.", ".$initialLocation->longitude.");
		
			var polyLine = new google.maps.Polyline({
				path: [endPos,beginPos],
				strokeColor: '#ff0000',
				strokeOpacity: 0.6,
				strokeWeight: 5
			});
			polyLine.setMap(map);";
		}
		$initialLocation=$location;
	}
	?>

}		
google.maps.event.addDomListener(window, 'load', initialize);	
</script>
<div id='googleMap' style="height:580px;width:800px;"></div>



			
			<?php
			echo "</td>";
		}
		echo "</tr>";
		echo "</table>";
		echo br(2);
	}
	

	if(isset($vehicleTable)){
		render_table($vehicleTable);
		echo br(2);
	}
	
	if(isset($routeID)){echo anchor("delete/route/$routeID","Delete Route","target='_blank'").br(2);};
	if(isset($routeID)){echo anchor("page/add_system/$routeID","Update Route").br();};
	
	echo form_open("",array("method"=>"GET"));
	echo form_dropdown("id",$routes,'0',"onChange=this.form.submit() class='form-control'");
	echo form_submit(array("value"=>"Submit","class"=>"form-control"));
	echo "</form>";
?>
