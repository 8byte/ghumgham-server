<?php
class Cost_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function add($data){
		return $this->db->insert("cost",$data);
	}
	function delete($data){
		return $this->db->delete("cost",$data);
	}
	function getAll(){
		return $this->db->get("cost")->result();		
		
	}
	function update($id,$data){
		$this->db->where("ID",$id);
		$this->db->update("cost",$data);
	}
	
	function getByID($id,$mode="object"){
		//$mode determines whether the return object is object or array
		$this->db->where("ID",$id);
		$res=$this->db->get("cost");
		if($res->num_rows()){
			if($mode=="object")
				$res=$res->result();
			else
				$res=$res->result_array();
			return $res[0];
		}
		return NULL;
	}
	
	
	function exists($id){
		$this->db->where("ID",$id);
		return ($this->db->get("cost")->num_rows()>0);
	}

	function existsCostBetween($place1,$place2){
		$this->db->where(array("place1"=>$place1,"place2"=>$place2));
		if($this->db->get("cost")->num_rows()>0)return true;
		$this->db->where(array("place2"=>$place1,"place1"=>$place2));
		if($this->db->get("cost")->num_rows()>0)return true;
		return false;
	}

	function existsCosts($array){
		foreach ($array as $arrayItem){
			if (!($this->existsCost($arrayItem))) return false;
		}
		return true;
	}

}

?>