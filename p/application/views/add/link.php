<?php
echo heading("Add link",3);
echo validation_errors();
if(isset($success)){echo heading($success,4).br();}

echo form_open("page/add_link");
echo "Place 1:".form_dropdown("place1",dropdown_array_of_locations($res->locationHandler->getAllLocations()),'',"id='a1' class='form-control places'").br();
echo "Place 2:".form_dropdown("place2",dropdown_array_of_locations($res->locationHandler->getAllLocations()),'',"id='a2' class='form-control places'").br();

echo form_submit(array("value"=>"Submit form","class"=>"form-control")).br();
echo "</form>";
?>

<script>
    $(document).on("ready",function(){
        $(".places").val("");

    })
</script>