
<?php
echo heading("All Vehicles",3);
if(isset($success)) echo "<h3>$success</h3>";

echo validation_errors();
?>

<script>
//var myarray=

function createSelectOfVehicleType(id,vTypeID,tableCell){
	var select=document.getElementById('vehicleTypeSelect'+id);
	//alert(select);
	<?php print php_dropdown_to_js_eval($vehicleTypes,"name");?>
	
	//self.getElementByName
	tableCell.appendChild(select);
	select.id="vehicleTypeSelect"+id;
	var i;
	
	for(i=0;i<select.options.length;i++){
		if (select.options[i].value==vTypeID){
			select.selectedIndex=i;
		}else{
			
		}
	}
	
}

function createSelectOfRoute(id,vRouteID,tableCell){
	var select=document.getElementById('vehicleRouteSelect'+id);
	<?php print php_dropdown_to_js_eval($routes,"name");?>
	
	tableCell.appendChild(select);
	select.id="vehicleRouteSelect"+id;
	select.selectedIndex=id;
	
	for(i=0;i<select.options.length;i++){
		if (select.options[i].value==vRouteID){
			select.selectedIndex=i;
		}
	}
}

function confirmdeletion(vID){
	if(confirm("Do you want to delete?")){
		window.open("<?php echo site_url("deletevehicle?id=");?>"+vID);
		
		var element = document.getElementById("tRow"+vID);
		element.parentNode.removeChild(element);
	}else{
	}
}
</script>

<table border=1 class="table">
<tr>
<th>ID</th><th>Name</th><th>Desc</th><th>vehicleType</th><th>route</th></tr>

<?php
if(!isset($array)) die("array not set");

for($i=0;$i<sizeof($array);$i++){
	echo "<tr id='tRow".$array[$i]->ID."'>\n";
	echo form_open("show/vehicle");
	
	echo "<td>".$array[$i]->ID.form_input(array("type"=>"hidden","name"=>"ID","value"=>$array[$i]->ID))."</td>\n";
	echo "<td>".form_input(array("type"=>"text","name"=>"name","value"=>$array[$i]->name,"class"=>"form-control"))."</td>\n";
    echo "<td>".form_input(array("type"=>"text","name"=>"desc","value"=>$array[$i]->desc,"class"=>"form-control"))."</td>\n";
	echo "<td id='vType".$array[$i]->ID."'><select name='vType' class='form-control' id=vehicleTypeSelect".$array[$i]->ID."></select></td>\n";
	echo "<td id='vRoute".$array[$i]->ID."'><select name='vRoute' class='form-control' id=vehicleRouteSelect".$array[$i]->ID."></select></td>\n";
	echo "<td>".form_submit(array("value"=>"Update","class"=>"form-control"))."</td>";
	echo "<td><a href='javascript:confirmdeletion(".$array[$i]->ID.")' url='".site_url("delete/vehicle/".$array[$i]->ID)."' >Delete</a>"."</td>";
	echo "\n</form>\n";
	echo "<script>createSelectOfVehicleType(".$array[$i]->ID.",".$array[$i]->typeID.",document.getElementById(\"vType".$array[$i]->ID."\"));</script>\n";
	echo "<script>createSelectOfRoute(".$array[$i]->ID.",".$array[$i]->routeID.",document.getElementById(\"vRoute".$array[$i]->ID."\"));</script>\n";
	echo "</tr>\n";
	
}
?>
</table>
