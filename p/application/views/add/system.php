<script>
    var GET_LINKS_URL="<?php echo site_url("get/linksOf/");?>";

function setClickListener(name){
		google.maps.event.addListener(name, 'click', function() {
			var select=document.getElementById('a'+this.RoadwayLocationIndex);
			
			select.value=this.RoadwayLocationID;
			
			//clear the map
			var i=0;
			var currentMarker;
			for (var key in markers['index'+this.RoadwayLocationIndex]) {
				currentMarker=markers['index'+this.RoadwayLocationIndex][key];
//				console.log("key is");
//				console.log(key);
				
				if (currentMarker.RoadwayLocationID==this.RoadwayLocationID) {
					
//					console.log("I have got it");
//					console.log(currentMarker);
					currentMarker.setMap(null);
					continue;
				}else{
//					console.log("compare begin");
//					console.log(currentMarker.RoadwayLocationID);
//					console.log(this.RoadwayLocationID);
//					console.log("compare end");
					//delete all of the unnecessary current points
					currentMarker.setMap(null);
					delete markers['index'+this.RoadwayLocationIndex][key];
				}
			}
			
			if(count>1){
					
					for (var key in markers['index'+(count-1)])
						oldMarker=markers['index'+(count-1)][key];
					
					var key=0;
					var newMarker;
//					console.log(this.RoadwayLocationIndex);
					for(key in markers['index'+(this.RoadwayLocationIndex)])
						newMarker=markers['index'+(this.RoadwayLocationIndex)][key];
					
//					console.log(oldMarker);
//					console.log(newMarker);
					var route=[oldMarker.position,newMarker.position];
					var polyLine = new google.maps.Polyline({
						path: route,
						strokeColor: "#ff0000",
						strokeOpacity: 0.6,
						strokeWeight: 5
					});
					polyLine.setMap(map);
					//polyLines.push(polyLine);
//					console.log( "created polyline");
				}

		
			//call the changed_Select function
			changed(document.getElementById('a'+this.RoadwayLocationIndex));
			
		});
	
}

function initialize() {
  map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
  
  //load all data from the first dropdown
  <?php
  echo "var latLng;\n";
	echo "markers=[];\n";
	echo "polyLines=[];\n";
	echo "markers['index1']={};\n";
	foreach($this->locationHandler->getAllLocations() as $location){
		$id=$location->ID;
		$markerName="markers['index1']['id".$id."']";
		
		//echo "var $markerName;\n";
		
		//print_r($location);
		echo "latLng = new google.maps.LatLng(".$location->latitude.", ".$location->longitude.");

		$markerName=new google.maps.Marker({
		  position:latLng,
		  RoadwayLocationID:".$location->ID.",
		  RoadwayLocationIndex:1
		  });
		 
		setClickListener($markerName);
		$markerName.setMap(map);
		";
	}
	?>

}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<table>
<tr>
<td>
<?php
echo heading("add system",3);
echo validation_errors();
if(isset($success)){echo heading($success,4).br();}

echo "<span ><span id=boxes name=helloworld>";
echo form_dropdown("a1",dropdown_array_of_locations($res->locationHandler->getAllLocations()),'',"id='a1' onChange='changed(this)' class='form-control'");

echo "</span>";
echo "<a href='#' onClick='selectCurrent()'>Select current</a>";
echo "</span>";

echo br()."<a href='#' onClick='deleteLast()'>Delete last</a>".br();


echo form_open("page/add_system",array("id"=>"form","name"=>"form"));
echo "Route Description".form_input(array(
        "name"=>"routeDescription",
        "required"=>"true",
        "class"=>"form-control")).br();
echo "ID (optional)".form_input(array("name"=>"routeID","value"=>$updateID,"class"=>"form-control")).br();

echo "</form>";
echo "<br /><button onClick='submitData()' class='form-control'>Submit</button>";
?>
</td>
<td valign="top" >
<div id='googleMap' style="padding-top:80px;height:580px;width:800px;"></div>
</td>
</tr>
</table>


<script>
var count=1;

function selectCurrent(){
	changed(document.getElementById("a"+count));
}
function deleteLast(){
	var select=document.getElementById('a'+count);
	//alert(select.options[3].value);
	select.parentNode.removeChild(select);	
	count--;
}
function submitData(){
	var form=document.getElementById("form");

    $(form).find(".location").remove();

	for(var i=1;i<=count;i++){
		var box=document.getElementById("a"+i);
		var id=box.options[box.selectedIndex].value;
		
		var item=document.createElement("input");
		item.type="hidden";
        $(item).addClass("location");
		item.name="places[]";
		item.value=id;
		
		form.appendChild(item);
	}

    var first=parseInt($(form).find(".location").first().val());
    var last=parseInt($(form).find(".location").last().val());
    var length=parseInt($(form).find(".location").length);

    if(length<3){
        alert("too short");
        return;
    }

    if(first != last){
        alert("first and last must be the same");
        return;
    }


	form.submit();
}


function changed(dropdown){
	//console.log("called");
	curItem=dropdown.options[dropdown.selectedIndex];
	
	var id=dropdown.id;
	id=parseInt(id.slice(1));
	
	
	//remove all indexes that are after current index
	if(id<count){
		
		for(;count>=id;count--){
//			console.log(count);
			var node=document.getElementById("a"+count);
			node.parentNode.removeChild(node);
			//and from map too
			//(*.*) bug here
			
			/*for (var key in markers['index'+count]) {
				currentMarker=markers['index'+count][key];
				currentMarker.setMap(null);	
			}*/
		}
	}
	
	//get all places that the current id is linked with

	var jqxhr=$.get(GET_LINKS_URL+"/"+curItem.value,function(){
	
	}).done(function(data){
		//the received data lies in the DATA variable, now render it
//		console.log(data);
//        console.log("DONE");

		places=[];
		
		var jsonDoc=JSON.parse(data);
		
		
		count++;
		
		var box=document.getElementById("boxes");
		var select=document.createElement("select");
        $(select).addClass("form-control");
//        console.log(select);
		select.name="a"+count;
		select.id=select.name;
		markers['index'+count]=[];
		$.each(jsonDoc,function(idx,place){
//				console.log("in each");
			
				var ID=parseInt(place.ID);
				var name=place.name;
				var latitude=place.latitude;
				var longitude=place.longitude;
				var district=place.district;
				
				latLng = new google.maps.LatLng(latitude,longitude);
				
				var myMarker=new google.maps.Marker({
					position:latLng,
					RoadwayLocationID:ID,
					RoadwayLocationIndex:count
				});
				
				setClickListener(myMarker);
			  
				myMarker.setMap(map);
				markers['index'+count]['id'+ID]=myMarker;
				var option=document.createElement("option");
				
				option.value=parseInt(ID);
				option.innerHTML=ID+". "+name+" ("+latitude+","+longitude+")";
				
				select.appendChild(option);
			});
			
			box.appendChild(select);
			$("#"+select.id).change(function(){
				changed(this);
			});
		});
		
	
	
	//remove the current locatoin from the new location list
	//render it
}

</script>

