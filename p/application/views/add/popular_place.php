<?php
echo heading("Add Popular place",3);
echo validation_errors();
if(isset($success)){echo heading($success,4).br();}
echo form_open("page/add_popular_place",array("enctype"=>"multipart/form-data"));

echo "Name: ".form_input(array("type"=>"text","name"=>"name","class"=>"form-control")).br();
echo "Place desc: ".form_textarea(array("type"=>"text","name"=>"desc","class"=>"form-control")).br();
echo "Category: ".form_dropdown("category",$res->popularCategories,"",'class="form-control"').br();
echo "Latitude: ".form_input(array("type"=>"text","name"=>"latitude","class"=>"form-control")).br();
echo "Longitude: ".form_input(array("type"=>"text","name"=>"longitude","class"=>"form-control")).br();

echo form_submit(array("value"=>"Submit form","class"=>"form-control")).br();
echo "</form>";
?>