<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<html lang="en">
<head>
    <title><?php if(isset($title))echo $title;else echo "Page"; ?></title>
    <?php echo meta("author","Damodar Dahal");?>
    <?php echo meta("description","Bus route navigation system over kathmandu");?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <?php
    if(isset($cacheThisPage) and $cacheThisPage==true){

    }else{
        ?>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
    <?php } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Le styles -->
    <!--    --><?php //echo link_tag('resources/css/bootstrap.min_cyborg.css');?>
    <style>body {padding-top: 60px;padding-bottom: 60px;}</style>
    <!--    --><?php //echo link_tag('resources/css/bootstrap-responsive.min.css');?>

    <!--    <script src="--><?php //echo base_url("/resources/js/jquery.js");?><!--"></script>-->

<!--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://bootswatch.com/darkly/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->

    <script>
        function redirectAfterConfirm(a){if(confirm('Delete this for sure?'))window.location=a;};
    </script>
</head>

<body >

<?php if(!isset($hideTopNavBar)){?>
    	<div id="wrap">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <?php echo anchor("page","GhumGham",array("class"=>"navbar-brand"));?>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Add <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><?php echo anchor("page/add_vehicle","Vehicle"); ?></li>
                            <li><?php echo anchor("page/add_location"," Location"); ?></li>
                            <li><?php echo anchor("page/add_system"," Route"); ?></li>
                            <li><?php echo anchor("page/add_link"," Roadway(connect places)"); ?></li>
                            <li><?php echo anchor("page/add_cost", "Cost"); ?></li>
                            <li><?php echo anchor("page/add_popular_place","Popular Place"); ?></li>
                            <li><?php echo anchor("page/add_popular_place_img","Popular Place Image"); ?></li>
                        </ul>

                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle"  data-toggle="dropdown">Show
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><?php echo anchor("show/routeLocation","Route Info"); ?></li>
                            <li><?php echo anchor("show/vehicle","Vehicles");?></li>
                            <li><?php echo anchor("show/roadway","Roadways");?></li>
                            <li><?php echo anchor("show/location","Locations");?></li>
                            <li><?php echo anchor("show/cost","Costs");?></li>
                            <li><?php echo anchor("show/popular_place","Popular Places");?></li>
                        </ul>
                    </li>
                    <li>
                        <?php echo anchor("page/api","Get API");?>
                    </li>
                    <li>
                        <?php echo anchor("get/multiverse","Get Multiverse");?>
                    </li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <?php echo anchor("logout",'<span class="glyphicon glyphicon-log-out"></span> Logout</a>');?></li>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
</div>
<?php if(isset($loadGoogleMapAPI)){
?>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        var mapProp = {
            center:new google.maps.LatLng(27.7000,85.3333),
            zoom:13,
            mapTypeId:google.maps.MapTypeId.ROADMAP

        };
    </script>
<?php
}
    ?>
<?php } ?>
<div class="container">
	
	
