<script>
    var marker=null;
    function initialize() {

        map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

        google.maps.event.addListener(map, "click", function (e) {
            console.log("done");
            var latLng = e.latLng;
            document.getElementById("latitude").value=latLng.lat();
            document.getElementById("longitude").value=latLng.lng();

            if(marker==null){
                marker=new google.maps.Marker({
                    position:latLng,
                    draggable:true,
                });

                marker.setMap(map);

                google.maps.event.addListener(marker, "drag", function (e) {
                    var latLng=e.latLng;
                    document.getElementById("latitude").value=latLng.lat();
                    document.getElementById("longitude").value=latLng.lng();

                });
            }else{
                marker.setPosition(e.latLng);
            }


        });


    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<table>
    <tr>
        <td>
            <?php
            echo heading("add location",3);
            echo validation_errors();
            if(isset($success)){echo heading($success,4).br();}
            //page/add_location
            echo form_open("???",array("id"=>"AddLocationForm"));
            echo form_input(array(
                    "type"=>"text",
                    "placeholder"=>"Name",
                    "name"=>"name",
                    "id"=>"lName",
                    "required"=>"true",
                    "class"=>"form-control")).br();

            echo form_input(array(
                "type"=>"float",
                "placeholder"=>"Latitude",
                "name"=>"latitude",
                "required"=>"true",
                "id"=>"latitude",
            ));

            echo form_input(array(
                "type"=>"float",
                "placeholder"=>"Longitude",
                "name"=>"longitude",
                "required"=>"true",
                "id"=>"longitude",

            ));
            echo "<button type=button id=resetBtn style='color:red;' >Reset</button>".br(2);
            echo is_bus_stop_form_field("isBusStop").br();

            echo "Connections:".form_dropdown("locations[]",dropdown_array_of_locations($res->locationHandler->getAllLocations()),"",'  class="form-control conns"');
            echo form_dropdown("locations[]",dropdown_array_of_locations($res->locationHandler->getAllLocations()),"",'  class="form-control conns"');
            echo form_dropdown("locations[]",dropdown_array_of_locations($res->locationHandler->getAllLocations()),"",'  class="form-control conns"');
            echo form_dropdown("locations[]",dropdown_array_of_locations($res->locationHandler->getAllLocations()),"",'  class="form-control conns"');
            echo form_dropdown("locations[]",dropdown_array_of_locations($res->locationHandler->getAllLocations()),"",'  class="form-control conns"');

            echo form_submit(array("value"=>"Submit form","class"=>"form-control")).br();
            echo "</form>";
            ?>

        </td>
        <td>
            <div id="googleMap" style="width:800px;height:580px;"></div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $('#AddLocationForm').submit(function(e){
            e.preventDefault();

            $.ajax({
                type: 'POST',
                cache: false,
                url: "<?php echo site_url("page/add_location/ajax");?>",
                data: 'id=header_contact_send&'+$(this).serialize(),
                success: function(msg) {
                    alert(msg);

                    resetForm();

                    downloadAndSetLocations();
                }
            });
        });

        $("#resetBtn").on("click",function(){
            $("#lName").val("");

            $("#AddLocationForm .conns").val("");
        })

        var resetForm=function(){
            $("#resetBtn").click();
        }



        var downloadAndSetLocations=function(){
            $.ajax({
                type:"GET",
                url:"<?php echo site_url("get/locationsSelect");?>",
                success:function(msg){
                    var downloaded=$(msg);
                    $("#AddLocationForm .conns").children().remove();

                    var parents=$("#AddLocationForm .conns");


                    downloaded.children().each(function(i,li){downloaded.prepend(li)})
                    var childs=downloaded.children();
                    for(var i=0;i<parents.length;i++){
                        $(parents[i]).append(childs.clone());
                    }

                    parents.val("");
                }
            })
        }

        resetForm();
        downloadAndSetLocations();
    });


</script>


