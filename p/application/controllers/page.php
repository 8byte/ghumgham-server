<?php
class Page extends CI_Controller
{
    function __construct($res=NULL){
        parent::__construct();
        $this->load->helper("loader");
        ensure_login();

    }

    function index(){
        load_page("home");
    }

    function add_cost(){
        $params=array();
        $params['title']="Add cost";

        $this->form_validation->set_rules("place1","Place 1","required|is_natural_no_zero|exists_location");
        $this->form_validation->set_rules("place2","Place 2","required|is_natural_no_zero|exists_location");
        $this->form_validation->set_rules("cost","cost","is_natural_no_zero|less_than[100]");
        $this->form_validation->set_rules("vType","vType","required|exists_vehicleType");

        if($this->form_validation->run()){

            $p1=set_value("place1")+0;
            $p2=set_value("place2")+0;
            $cost=set_value("cost")+0;
            $vType=set_value("vType")+0;
            if($cost<15)$cost=15;

            if($this->costHandler->existsCostBetween($p1,$p2)){
                $params['success']="Already added! can not re-add!";
            }elseif($p1==$p2){
                $params['success']="Place1 and Place2 must be different";
            }else{
                $this->costHandler->add(array("place1"=>$p1,"place2"=>$p2,"cost"=>$cost,"vehicleType"=>$vType));
                $params['success']="Cost added!";
            }

        }
        load_page("add/cost",$params);
    }

    function add_system($updateID=0){
        /*adds a route with its locations*/

        $params=array();
        $params['title']="Add route";
        $params['loadGoogleMapAPI']=TRUE;
        $array=$this->input->get_post("places");
        $params['updateID']=0;
        if(exists_route($updateID)){$params['updateID']=$updateID+0;}
        $this->form_validation->set_rules("routeDescription","Route Description","required|xss_clean");
        $this->form_validation->set_rules("routeID","Route ID","exists_route");

        if($array and $this->form_validation->run()){

            if(is_array($array) and sizeof($array)>1){

                //check if all locations exist
                if($this->locationHandler->existsLocations($array)){
                    $routeDesc=set_value("routeDescription");
                    $routeID=set_value("routeID");

                    $data=array("remarks"=>$routeDesc);
                    if($routeID>0){
                        //echo "update mode";
                        //echo $routeID;
                        $this->routeHandler->deleteRouteAndAssociations($routeID);
                        $data['ID']=$routeID+0;
                    }

                    $routeID=$this->routeHandler->addRouteAndReturnID($data);
                    $this->routeLocationHandler->addLocationsInRoute($array,$routeID);
                    $params['success']="'$routeDesc' route added!";
                }else echo "not all places exist.";
            }else{
                echo "invalid array";
            }
        }
        //print_r($_POST);
        load_page("add/system",$params);
    }

    function split_roadway($roadway=0){
        $params=array();
        $params['title']="Split roadway";
        $roadwayID=$roadway+0;
        if($this->roadwayHandler->existsRoadway($roadwayID)){
            $params['title']="Split roadway $roadwayID";
            $locations=$this->roadwayHandler->getLocationsOfRoadway($roadwayID);
            $point1=$this->locationHandler->getLocationByID($locations[0]);
            $point2=$this->locationHandler->getLocationByID($locations[1]);

            $this->form_validation->set_rules("breaker","Breaking point","required|exists_location");
            if($this->form_validation->run()){
                $breaker=set_value("breaker")+0;

                if ($point1->ID==$breaker or $point2->ID==$breaker)
                    echo "can not break this";//starting or ending point given as the break point
                else{
                    //check if there is already a route between (point1 to breaker ) or (point2 to breaker)
                    if ($this->roadwayHandler->existsRoadwayBetween($point1->ID,$breaker))
                        echo "roadway point1->breaker already exists. can not do what you wanted";
                    else if ($this->roadwayHandler->existsRoadwayBetween($point2->ID,$breaker)){
                        echo "roadway point2->breaker already exists. can not do what you wanted";
                    }else{
                        $this->routeLocationHandler->split_points($point1->ID,$point2->ID,$breaker);
                        $this->roadwayHandler->split_roadway($roadwayID,$breaker);
                        $params['success']=heading("SUCCESSFUL",3);
                    }
                }
            }

            $params['header']="Splitting the roadway between ".br().format_location($point1).br()." and ".br().format_location($point2).br(2);
            $params['roadwayID']=$roadwayID;

            load_page("split_roadway",$params);

        }else die("roadway does not exist");

    }

    function add_location($mode="default"){

        $params=array();
        $params['title']="Add location";
        $params['loadGoogleMapAPI']=TRUE;
        $this->form_validation->set_rules("latitude","Latitude","required|decimal|greater_than[-1]");
        $this->form_validation->set_rules("longitude","Longitude","required|decimal|greater_than[-1]");
        $this->form_validation->set_rules("name","PlaceName","required|xss_clean|max_width[".$this->config->config['db']['location']['name']['length']."]");
        $this->form_validation->set_rules("isBusStop","Bus stop","required|is_natural|less_than[2]");

        if($this->form_validation->run()){
            $name=set_value("name");
            $latitude=set_value("latitude")+0;
            $longitude=set_value("longitude")+0;
            $isBusStop=(set_value("isBusStop")+0)>1;

            $id=$this->locationHandler->addLocationAndReturnID(array("name"=>$name,
                "latitude"=>$latitude,
                "longitude"=>$longitude,
                "isStop"=>$isBusStop
            ));

            $locations=$this->input->get_post("locations");
            if($locations){
                for($i=0;$i<count($locations);$i++){
                    $p1=$id;
                    $p2=$locations[$i]+0;

                    if(exists_location($p2)){
                        if($this->roadwayHandler->existsRoadwayBetween($p1,$p2)){

                        }else{
                            if($p1+0==$p2+0){
//                                echo "repeated places";
                            }elseif($this->roadwayHandler->add(array("point1"=>$p1,"point2"=>$p2))){
//                                $params['success']= "link added";
                            };
                        }
                    }
                }
            }
            $params['success']=$name." added";
            $params['ajaxMessage']=$params['success'];
        }else{
            $params['ajaxMessage']=validation_errors();
        }
        if($mode=="ajax"){
            echo $params['ajaxMessage'];
        }
        else
            load_page("add/location",$params);
    }

    function add_popular_place(){
        $this->load->helper(array('form', 'url'));

        $params=array();
        $params['title']="Add popular place";
        $this->form_validation->set_rules("latitude","Latitude","required|decimal|greater_than[-1]");
        $this->form_validation->set_rules("longitude","Longitude","required|decimal|greater_than[-1]");
        $this->form_validation->set_rules("name","Place Name","required|max_width[100]");
        $this->form_validation->set_rules("category","Category","max_width[30]");
        $this->form_validation->set_rules("desc","Description","required|max_width[1000]");

        if($this->form_validation->run()){
            $name=set_value("name");
            $category=set_value("category");
            $latitude=set_value("latitude")+0;
            $longitude=set_value("longitude")+0;
            $desc=set_value("desc");

            $id=$this->popularPlaceHandler->add(array("name"=>$name,
                "category"=>$category,
                "latitude"=>$latitude,
                "description"=>$desc,
                "longitude"=>$longitude));



            $params['success']=$name." added";
        }
        load_page("add/popular_place",$params);
    }

    function add_popular_place_img(){
        $this->form_validation->set_rules("place","Popular Place","required|is_natural_no_zero|exists_popular_place");
        $params=array();
        $params['title']="Add popular place image";
        if($this->form_validation->run()){

            $id=set_value("place");

            $config=[];
            $config['upload_path'] = './resources/img/places/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['file_name']="$id.png";
            $config['overwrite']=TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload("file"))
            {
                $error = array('error' => $this->upload->display_errors());

                echo "Could not upload";

                var_dump($this->upload->display_errors());
            }
            else
            {
                $data= $this->upload->data();

                $id=$this->popularPlaceImageHandler->add(array(
                    "popular_place_id"=>$id,
                ));

                $from=$data['full_path'];

                $to="./resources/img/places/$id.jpg";

                rename($from,$to);

//                var_dump($data);
                $params['success']="Success";
                echo "DONE";
            }

        }

        load_page("add/popular_place_img",$params);
    }

    function add_link(){
        $params=array();
        $params['title']="Add roadway";
        $this->form_validation->set_rules("place1","Place 1","required|is_natural_no_zero|exists_location");
        $this->form_validation->set_rules("place2","Place 2","required|is_natural_no_zero|exists_location");
        if($this->form_validation->run()){
            $p1=set_value("place1");
            $p2=set_value("place2");

            if($this->roadwayHandler->existsRoadwayBetween($p1,$p2)){
                echo "already constructed!";
            }
            else{
                if($p1+0==$p2+0){
                    echo "repeated places";
                }elseif($this->roadwayHandler->add(array("point1"=>$p1,"point2"=>$p2))){
                    $pl1=$this->locationHandler->getLocationById($p1);
                    $pl2=$this->locationHandler->getLocationById($p2);

                    $params['success']= $pl1->name."->".$pl2->name." connected";
                };
            }
        }
        load_page("add/link",$params);
    }

    function add_vehicle(){
        $params=array();
        $params['title']="Add vehicle";

        $routes=array();
        $list1=$this->routeHandler->getAllRoutes();
        foreach($list1 as $item){
            $routes[$item->ID]=format_route($item);
        }

        $vehicleTypes=array();
        $list2=$this->vehicleTypeHandler->getAllTypes();
        foreach($list2 as $item){
            $vehicleTypes[$item->ID]=format_vehicleType($item);
        }
        $params['vehicleTypes']=$vehicleTypes;
        $params['routes']=$routes;

        $this->form_validation->set_rules("routeID","Route","required|is_natural_no_zero|exists_route");
        $this->form_validation->set_rules("vehicleTypeID","VehicleType","required|is_natural_no_zero|exists_vehicleType");
        $this->form_validation->set_rules("name","Vehicle name","required|max_len[".$this->config->config['db']['vehicle']['name']['length']."]");
        $this->form_validation->set_rules("desc","Vehicle desc","");

        if($this->form_validation->run()){
            $name=set_value("name");
            $routeID=set_value("routeID");
            $vTypeID=set_value("vehicleTypeID");
            $desc=set_value("desc");
            $this->vehicleHandler->addVehicle(array(
                "name"=>$name,
                "routeID"=>$routeID,
                "typeID"=>$vTypeID,
                "desc"=>$desc));


            $params['success']=$name." added!";
        }

        load_page("add/vehicle",$params);
    }

    function api(){
        $params=array();
        $params['title']="Create API request";

        $params['loadGoogleMapAPI']=TRUE;
        load_page("api",$params);
    }

}
?>