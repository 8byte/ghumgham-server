<?php
class VehicleType_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAllTypes(){
		return $this->db->get("vehicletype")->result();
	}
	function existsType($id){
		$this->db->where("ID",$id);
		return (sizeof($this->db->get("vehicletype")->result())>0);
	}
	function getVehicleTypesFor($vehicleID){
		$SQL="SELECT * from vehicletype where ID=(SELECT typeID from vehicle where ID=$vehicleID)";
		$result=$this->db->query($SQL)->result();
		return $result[0];
	}
}
?>