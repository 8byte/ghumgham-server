<?php
class Login extends CI_Controller
{
	function __construct($res=NULL){
		parent::__construct();
		$this->load->helper("loader");
	}
	

	public function logout($res=NULL){
		logOut();
	}
	
	
	public function admin_check($pass){
		if(!isset($this->username))return false;
		return matchesAdmin($this->username,$pass);
	}
	public function username_store($uname){
		$this->username=$uname;
		return true;
	}
	
	public function index($res=NULL){
		if(loggedIn()){redirect('page');}
		
		$this->load->helper(array("form","url"));
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules('username',"Username","required|max_length[32]|xss_clean|callback_username_store");
		$this->form_validation->set_rules('password',"Password","required|max_length[32]|callback_admin_check");
		
		$this->form_validation->set_message("admin_check","login_unsuccessful");
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		if($this->form_validation->run()){
			loginUser($this->username);
		}else{
			load_page("login",array("hideTopNavBar"=>TRUE),TRUE,FALSE);
		}
	}
}
?>