<?php
class RouteLocation_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function deleteLocationsOfRoute($id){
		//$id as string/num/anything
		//deletes the row with routeID=$id from routelocation table
		//returns the query result
		return $this->db->delete("route_location",array("routeID"=>$id));
	}
	
	function getAllPlacesThatGoFrom($locationID){
		$SQL="
		SELECT distinct(locationID)
		FROM route_location AS rl
		WHERE EXISTS
		( SELECT * 
		FROM route_location AS rl2
		WHERE rl.routeID = rl2.routeID  
		AND rl.orderID < rl2.orderID 
		AND rl2.locationID = $locationID 
		) and locationID<>$locationID
		";
		$result=$this->db->query($SQL);
		return $result->result();
	}
	function addLocationsInRoute($array,$routeID){
		//takes $array of locations in order of how the route goes.
		
		//note: the data go into the table sequentially,
		//and the order of the locations lying in the route
		//is determined solely by the SN of the route_roadway row
		
		//returns: query result
		$CI=&get_instance();
		$sql="insert into route_location(routeID,orderID,locationID) VALUES ";
		for($i=0;$i<sizeof($array);$i++){
			$sql.="($routeID,".($i+1).",".$array[$i].")";
			if(!($i==sizeof($array)-1))
				$sql.=",";
		}
		return $this->db->query($sql);
		
	}
	
	function getRouteLocationsOfRoute($routeID,$mode="object"){
		//returns: an array of locations IN ORDER 
		//of how they are in the database.
		$SQL="SELECT location.* from 
		route_location inner join location on route_location.locationID=location.ID 
		where route_location.routeID=$routeID order by orderID";
		
		$res=$this->db->query($SQL);
		if($mode=="object")
			return $res->result();
		elseif($mode=="array")
			return $res->result_array();
	}
	function getPlacesThatComeAfter($route,$place){
		//returns all locations that fall after the first occurance of $place in $route
		
		$SQL="SELECT distinct(locationID) from route_location where
		routeID=$route and
		orderID>(SELECT min(orderID) from route_location where routeID=$route and locationID=$place)";
		
		$result=array();
		foreach( $this->db->query($SQL)->result() as $item){
			array_push($result,$item->locationID);
		}
		return $result;
	}
	
	function getPlacesThatComeBefore($route,$place){
		//returns all locations that fall after the first occurance of $place in $route
		
		$SQL="SELECT distinct(locationID) from route_location where
		routeID=$route and
		orderID<(SELECT max(orderID) from route_location where routeID=$route and locationID=$place)";
		
		$result=array();
		foreach( $this->db->query($SQL)->result() as $item){
			array_push($result,$item->locationID);
		}
		return $result;
	}
	function getPlacesInBetween($routeID,$point1,$point2){
		//returns an array of places including $point1 and $point2 inclusive
		//the flow is in order
		
		$circle=RouteCircle::constructFromRouteID($routeID);
		
		return $circle->getPlacesInBetween($point1,$point2);
	}
	function getRoutesContainingLocations($point1,$point2){
		$sql="SELECT distinct(routeID) FROM route_location where locationID=$point1 and 
		routeID in (SELECT distinct(routeID) FROM route_location where locationID=$point2)";
		$array=array();
		foreach ($this->db->query($sql)->result() as $item){
			array_push($array,$item->routeID+0);
		}
		return $array;
	}
	
// 	function isPlace2BehindPlace1($route,$point1,$point2){
// 		//boolean to ensure that Point1 is before point2
// 		//echo $point1;
// 		//echo $route;
// 		$SQL="SELECT min(orderID) as pos from route_location where routeID=$route and locationID=$point1";
// 		$posOfPoint1=$this->db->query($SQL)->result()[0]->pos;
// 		//echo $SQL;
// 		$SQL="SELECT max(orderID) as pos from route_location where routeID=$route and locationID=$point2";
// 		$posOfPoint2=$this->db->query($SQL)->result()[0]->pos;
		
// 		return ($posOfPoint1<$posOfPoint2);
		
// 	}
	
	function getDistanceBetween($routeID,$from,$to){
		//returns the shortest distance between $from to $to in a route
		//NOTE: distance=sqrt(latitude^2+longitude^2)
		
		$distance=0;
		$locations=$this->getPlacesInBetween($routeID,$from,$to);
		assert(sizeof($locations)>0);
		
		$CI=&get_instance();
		$firstLocation=$locations[0];
		for($i=1;$i<sizeof($locations);$i++){
			$thisLocation=$locations[$i];
			$myDistance=$CI->locationHandler->getDistanceBetween($firstLocation,$thisLocation);
			$distance += $myDistance;
		}
		return $distance;
	}
	function getRoutesThatGoFrom($from,$to,$lengthFilter=FALSE){
		//returns routeID where $to comes after $from
		//if lengthFilter=true, it returns the route with shortest length only
		
		$result=array();
		
		
		$shortestDistance=100000000;
		$shortestRoute=0;
		$routesContainingPlaces=$this->routeLocationHandler->getRoutesContainingLocations($from,$to);
		foreach($routesContainingPlaces as $route){
			$circle=RouteCircle::constructFromRouteID($route);
			
			if($circle->goesFrom($from,$to)){
				array_push($result,$route);
				
				$distance=$circle->getDistanceBetweenPoints($from,$to);
				if($distance<$shortestDistance){
					$shortestDistance=$distance;
					$shortestRoute=$route;
				}
			}else{
// 				echo 'no';
			}
		}
		if($lengthFilter and sizeof($result)>0)
			return $shortestRoute;
		else
			return $result;
	}
	
	function split_points($point1,$point2,$breaker){
		//in table roadway_location, it finds routes that flow from $point1->$point2 or vice versa
		//then adds $breaker in between
		
		//for this, get all routes that contain $point1 and $point2. 
		//then use the query to find $point1->$point2. If such exists, run query
		//then again, use query to find $point2->$point1. If such exists, run query
		$routes=$this->getRoutesContainingLocations($point1,$point2);
		foreach ($routes as $route){
			//first from $point1->$point2
			$SQL="SELECT orderID from route_location where routeID=".$route." and locationID=$point1 and 
				(orderID+1) in (SELECT orderID from route_location where routeID=$route and locationID=$point2)";
			$result=$this->db->query($SQL);
			foreach ($result->result() as $item){
				$orderID=$item->orderID;
				//add orderID of items whose orderID>$orderID
				$SQL="UPDATE route_location SET orderID=orderID+1 where orderID>$orderID and routeID=$route ORDER BY routeID,orderID desc";
				$this->db->query($SQL);
				$SQL="INSERT INTO route_location (routeID,orderID,locationID) values($route,".($orderID+1).",$breaker)";
				$this->db->query($SQL);
			}
			
			//now from $point2->$point1
			$SQL="SELECT orderID from route_location where routeID=".$route." and locationID=$point2 and 
				(orderID+1) in (SELECT orderID from route_location where routeID=$route and locationID=$point1)";
			$result=$this->db->query($SQL);
			foreach ($result->result() as $item){
				$orderID=$item->orderID;
				//add orderID of items whose orderID>$orderID
				$SQL="UPDATE route_location SET orderID=orderID+1 where orderID>$orderID and routeID=$route ORDER BY routeID,orderID desc";
				$this->db->query($SQL);
				$SQL="INSERT INTO route_location (routeID,orderID,locationID) values($route,".($orderID+1).",$breaker)";
				$this->db->query($SQL);
			}
		}
	}
}
?>
